package com.wipro.catalogueservice.catalogueservice;

public class SKU {
	
	private Integer id;
	private Number productId;
	private String name;
	private String description;
	private Number price;
	private Number count;

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Number getProductId() {
		return productId;
	}
	public void setProductId(Number productId) {
		this.productId = productId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Number getPrice() {
		return price;
	}
	public void setPrice(Number price) {
		this.price = price;
	}
	public Number getCount() {
		return count;
	}
	public void setCount(Number count) {
		this.count = count;
	}
	
}
