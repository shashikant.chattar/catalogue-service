package com.wipro.catalogueservice.catalogueservice;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.concurrent.atomic.AtomicInteger;

@RestController
public class CatalogueController {

	static AtomicInteger ID = new AtomicInteger();
	ArrayList<Product> products = new ArrayList<Product>();
	
	@Autowired
	RestTemplate restTemplate;
	
	@GetMapping("/products")
	@CrossOrigin(origins = "*")
	public ResponseEntity<Object> getProducts() throws IOException {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Content-Type", "application/json");
		return new ResponseEntity<>(products,headers,HttpStatus.OK);	
	}
	
	@PostMapping("/products")
	@CrossOrigin(origins = "*")
	public ResponseEntity<Object> addProduct(@RequestBody Product p) throws IOException {
		HttpHeaders headers = new HttpHeaders();
		
		if(p == null 
				|| p.getCatalog() == null
				|| p.getClass() == null
				|| p.getName() == null
				|| RequestUtils.emptyOrMissing(p.getName())
				|| RequestUtils.emptyOrMissing(p.getCatalog())) 
		{
			headers.set("Content-Type", "text/plain");
			return new ResponseEntity<>("Catalog name cannot be empty",headers,HttpStatus.BAD_REQUEST);
		}
		else {
			Product p1 = new Product();
			p1.setId(ID.incrementAndGet());
			p1.setCatalog(p.getCatalog());
			p1.setName(p.getName());
			products.add(p1);
			headers.set("Content-Type", "application/json");
			return new ResponseEntity<>(p1,headers,HttpStatus.CREATED);		
		}	
	}
	
	@PutMapping("/products/{id}")
	@CrossOrigin(origins = "*")
	public ResponseEntity<Object> updateProduct(@PathVariable Integer id,@RequestBody Product p) throws IOException {
		HttpHeaders headers = new HttpHeaders();
		
		Boolean update = false;
		
		for(Product p1: products) {
			if(p1.getId() == id) {
				
				if(p == null 
						|| p.getCatalog() == null
						|| p.getClass() == null
						|| p.getName() == null) 
				{
					headers.set("Content-Type", "text/plain");
					return new ResponseEntity<>("catalog is mandatory",headers,HttpStatus.BAD_REQUEST);
				}
				
				p1.setCatalog(p.getCatalog());
				p1.setName(p.getName());
				headers.set("Content-Type", "application/json");
				update = true;
				return new ResponseEntity<>(p1,headers,HttpStatus.OK);	
			}
		}
		
		if(update == false) {
			headers.set("Content-Type", "text/plain");
			return new ResponseEntity<>("Product not found",headers,HttpStatus.NOT_FOUND);				
		}
		
		return new ResponseEntity<>(p,headers,HttpStatus.OK);	
	}
	
	@DeleteMapping("/products/{id}")
	@CrossOrigin(origins = "*")
	public ResponseEntity<Object> deleteProduct(@PathVariable Integer id) throws IOException {
		HttpHeaders headers = new HttpHeaders();
		
		Boolean delete = false;
		
		for(Product p1: products) {
			if(p1.getId() == id) {
				products.remove(p1);
				headers.set("Content-Type", "application/json");
				delete = true;
				return new ResponseEntity<>(p1,headers,HttpStatus.OK);	
			}
		}
		
		if(delete == false) {
			headers.set("Content-Type", "text/plain");
			return new ResponseEntity<>("Product not found",headers,HttpStatus.NOT_FOUND);				
		}
		
		return new ResponseEntity<>("Product not found",headers,HttpStatus.NOT_FOUND);	
	}
	
	@GetMapping("/products/{id}")
	@CrossOrigin(origins = "*")
	public ResponseEntity<Object> getProduct(@PathVariable Integer id) throws IOException {
		HttpHeaders headers = new HttpHeaders();
		
		Boolean found = false;
		for(Product p1: products) {
			if(p1.getId() == id) {
				headers.set("Content-Type", "application/json");
				found = true;
				ProductDetail pd = new ProductDetail(p1);
				pd.setSkus(getSKUs(pd.getId()));
				headers.set("Content-Type", "application/json");
				return new ResponseEntity<>(pd,headers,HttpStatus.OK);	
			}
		}
		
		if(found == false) {
			headers.set("Content-Type", "text/plain");
			return new ResponseEntity<>("Product not found",headers,HttpStatus.NOT_FOUND);				
		}
		return new ResponseEntity<>("Product not found",headers,HttpStatus.NOT_FOUND);
		
	}

	private List<SKU> getSKUs(Integer productId) {
		return restTemplate.getForEntity("http://inventory-service/skus?productId="+productId, List.class).getBody();
	}
	
	
}
