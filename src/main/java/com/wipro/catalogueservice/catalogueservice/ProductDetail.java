package com.wipro.catalogueservice.catalogueservice;

import java.util.List;

public class ProductDetail extends Product{
	public List<SKU> getSkus() {
		return skus;
	}

	public void setSkus(List<SKU> skus) {
		this.skus = skus;
	}

	List <SKU> skus;

	public ProductDetail(Product p) {
		super();
		this.setCatalog(p.getCatalog());
		this.setName(p.getName());
		this.setId(p.getId());
	}
}
