FROM openjdk:8-jre-alpine
RUN mkdir /app
COPY build/libs/*.jar /app
RUN mv `ls /app/*.jar | head -n 1` /app/app.jar
WORKDIR /app
ENTRYPOINT ["java","-jar","/app/app.jar"]